import random

pw_wordlist_file = "pw_words.txt"
pw_length = 4
number_of_generated_passwords = 10
brute_force_pws_per_second = 1000000000000

words = []

with open(pw_wordlist_file) as file:
    for line in file:
        words.append(line.strip())

number_of_words = len(words)


def print_information():
    number_of_combinations = number_of_words ** pw_length
    duration_for_brute_force = number_of_combinations / (brute_force_pws_per_second * 60 * 60 * 24 * 356 * 2)
    avg_word_length = int(sum([len(word) for word in words]) / len(words))

    print("Number of words: " + str(number_of_words))
    print("Number of combinations: " + "{:.2e}".format(number_of_combinations))
    print("Average word-length: " + str(avg_word_length))
    print("Average password-length: " + str(avg_word_length * pw_length + pw_length - 1))
    print(
        "Minimal duration for brute force in years (100k attempts per second; 50% of all combinations): " + "{:.2e}".format(
            duration_for_brute_force))


def generate_password(length: int):
    password = ""

    for i in range(0, length):
        password = password + " " + words[random.randint(0, number_of_words - 1)]

    return password


print_information()

print("\nNr.\t| Brute force\t| Length\t| Generated PW\n"
      "\t| (years)\t\t|\t\t\t|")

line = "----+---------------+-----------+--------------------------------------------------------------------------"

print(line)

for i in range(0, number_of_generated_passwords):
    index = i + 1

    generated_password = generate_password(pw_length)

    brute_force_duration = 54 ** len(generated_password) / (brute_force_pws_per_second * 60 * 60 * 24 * 356 * 2)

    print(str(index) + "\t| " + "{:.2e}".format(brute_force_duration) + "\t\t| " + str(
        len(generated_password) - 1) + "\t\t|" + generated_password)
print(line)

print("The brute force duration in this context is calculated based on the length of the pw itself.")
